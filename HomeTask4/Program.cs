﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HomeTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            int dirCount = 0;
            while (true)
            {
                string dirPath = Console.ReadLine();
                if (Directory.Exists(dirPath))
                {
                    if (!(dirPath.EndsWith(@"\")))
                    {                       
                        dirPath = String.Format($@"{dirPath}\");
                    }
                    string[] dirTree = Directory.GetDirectories(dirPath);
                    Array.Sort(dirTree);
                    foreach (string dirName in dirTree)
                    {
                        dirCount++;
                        Console.WriteLine($"{dirCount}. {dirName}");
                    }
                    while (true)
                    {
                        try
                        {
                            Console.WriteLine($"Select the folder number in the directory {dirPath}");
                            Int32 directoryNumber = Convert.ToInt32(Console.ReadLine());
                            if (directoryNumber > 0 && directoryNumber <= dirCount)
                            {
                                directoryNumber = directoryNumber - 1;
                                try
                                {
                                    if (Directory.Exists(dirTree[directoryNumber]))
                                    {
                                        string[] dirFiles = Directory.GetFiles(dirTree[directoryNumber]);
                                        Array.Sort(dirFiles);
                                        if (dirFiles.Length == 0)
                                        {
                                            Console.WriteLine("Folder is empty");
                                        }
                                        foreach (string dirName in dirFiles)
                                        {
                                            Console.WriteLine(dirName);
                                        }
                                    }
                                    
                                }
                                catch (UnauthorizedAccessException)
                                {
                                    Console.WriteLine($"Access denied to folder {dirTree[directoryNumber]}");
                                }
                            }
                            else
                            {
                                Console.WriteLine($"To open, select a folder from 1 to {dirCount}");
                            }
                        }
                        catch
                        {
                            Console.WriteLine("To open, select the correct folder number");
                        }
                    }

                }
                else
                {
                    Console.WriteLine("Invalid directory path selected");
                }
            }
             
        }
    }
}

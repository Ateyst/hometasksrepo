﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace HomeTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "textSample.txt");
            if (File.Exists(filePath))
            {
                string textFile = File.ReadAllText(filePath);
                if (textFile.Trim().Length != 0)
                {
                    string[] textArray;
                    string every10Word;
                    every10Word = "";
                    Regex regex = new Regex("[^a-zA-Zа-яА-Я \n]");

                    textFile = regex.Replace(textFile, "");
                    regex = new Regex(@"\s+");
                    textFile = regex.Replace(textFile, @" ");

                    textFile=textFile.Trim();
                    textArray = textFile.Split(' ');
                    Console.WriteLine(textArray.Length);
                    for (int i = 9; i < textArray.Length; i+=10)
                        every10Word = $"{every10Word}{textArray[i]},";
                    Console.WriteLine(every10Word.TrimEnd(','));
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("File 'textSample.txt' is empty");
                    Console.ReadKey();
                }

            }
            else
            {
                Console.WriteLine("File 'textSample.txt' wasnt found in the project folder");
                Console.ReadKey();
            }
        }
    }
}

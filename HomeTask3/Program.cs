﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HomeTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "textSample.txt");
            if (File.Exists(filePath))
            {
                string textFile = File.ReadAllText(filePath);
                if (textFile.Trim().Length != 0)
                {
                    string[] textArray;
                    char[] delimiterChars = { '.', '!', '?' };
                    textFile = textFile.Trim();
                    textArray = textFile.Split(delimiterChars);
                    if (textArray.Length > 3)
                    {
                        Console.WriteLine(textArray[2].Reverse().ToArray());
                    }
                    else
                    {
                        Console.WriteLine("Not enough sentences in the text");
                    }
                    
                }
                else
                {
                    Console.WriteLine("File 'textSample.txt' is empty");
                }

            }
            else
            {
                Console.WriteLine("File 'textSample.txt' wasnt found in the project folder");                
            }
            Console.ReadKey();
        }
    }
}

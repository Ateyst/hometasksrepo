﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HomeTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "textSample.txt");
            var test = "";
            if (File.Exists(filePath))
            {
                string textFile = File.ReadAllText(filePath);
                if (textFile.Trim().Length != 0)
                {
                    Console.WriteLine(textFile);
                    Console.WriteLine("Select symbol to delete");
                    string replaceSymbol = Console.ReadLine();
                    if (textFile.IndexOf(replaceSymbol) != -1)
                    {
                        textFile = textFile.Replace(replaceSymbol, null);
                        File.WriteAllText(filePath, textFile);
                        Console.WriteLine($"Symbol '{replaceSymbol}' was deleted and saved to file");                        
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine($"Symbol '{replaceSymbol}' wasnt found");
                        Console.ReadKey();
                    } 

                }
                else
                {
                    Console.WriteLine("File 'textSample.txt' is empty");
                    Console.ReadKey();
                }
                    
            }
            else
            {
                Console.WriteLine("File 'textSample.txt' wasnt found in the project folder");
                Console.ReadKey();
            }            
        }
    }
}
